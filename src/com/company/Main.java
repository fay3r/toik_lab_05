package com.company;

public class Main {

    public static void main(String[] args) {
        StackOperationsImpl stack = new StackOperationsImpl();

        stack.push("raz");
        stack.push("dwa");
        stack.push("trzy");
        stack.push("cztery");
        stack.push("piec");

        System.out.println(stack.get());

        System.out.println(stack.pop());
        System.out.println(stack.get());

        System.out.println(stack.pop());
        System.out.println(stack.get());

        System.out.println(stack.pop());
        System.out.println(stack.get());

        System.out.println(stack.pop());
        System.out.println(stack.get());

        System.out.println(stack.pop());
        System.out.println(stack.get());
    }
}
